import ApolloBoost, { gql } from "apollo-boost";

const client = new ApolloBoost({
  uri: "https://graphql-session-2-app-activity.herokuapp.com/",
});

const users = gql`
  query {
    getUsers {
      firstName
      lastName
      email
    }
  }
`;

const items = gql`
  query {
    getItems {
      name
      description
      unitPrice
      categoryId
    }
  }
`;

const categories = gql`
  query {
    getCategories {
      name
      description
    }
  }
`;

const getAllUsers = () => {
  client.query({ query: users }).then((response) => {
    const users = response.data.getUsers;
    console.log(users);
    let html = "";
    for (let i = 0; i <= users.length - 1; i++) {
      html += `
              <div class="card m-2" class="movie-card">
                  <div class="card-body">
                      <h3 class="card-title" data-id=${users[i].id} id="movie-title">${users[i].firstName} ${users[i].lastName}</h3>
                      <p class="badge badge-primary px-2 py-1">${users[i].email}</p>
                  </div>
              </div>
              `;
    }
    document.querySelector("#users").innerHTML = html;
    document.querySelector("#items").innerHTML = null;
    document.querySelector("#categories").innerHTML = null;
  });
};

const getAllItems = () => {
  client.query({ query: items }).then((response) => {
    const items = response.data.getItems;
    let html = "";
    for (let i = 0; i <= items.length - 1; i++) {
      html += `
                <div class="card m-2">
                    <div class="card-body">
                        <h3 class="card-title">${items[i].name}</h3>
                        <p class="badge badge-primary px-2 py-1">${items[i].description}</p>
                        <p class="badge badge-primary px-2 py-1">${items[i].unitPrice} PHP</p>
                        <p class="badge badge-primary px-2 py-1">ID: ${items[i].categoryId}</p>
                    </div>
                </div>
                `;
    }
    document.querySelector("#users").innerHTML = null;
    document.querySelector("#items").innerHTML = html;
    document.querySelector("#categories").innerHTML = null;
  });
};

const getAllCategories = () => {
  client.query({ query: categories }).then((response) => {
    const categories = response.data.getCategories;
    let html = "";
    for (let i = 0; i <= categories.length - 1; i++) {
      html += `
                <div class="card m-2">
                    <div class="card-body">
                        <h3 class="card-title">${categories[i].name}</h3>
                        <p class="badge badge-primary px-2 py-1">${categories[i].description}</p>
                    </div>
                </div>
                `;
    }
    document.querySelector("#users").innerHTML = null;
    document.querySelector("#items").innerHTML = null;
    document.querySelector("#categories").innerHTML = html;
  });
};

getAllUsers();

document.querySelector("#users-link").addEventListener("click", () => {
  getAllUsers();
});

document.querySelector("#items-link").addEventListener("click", () => {
  getAllItems();
});
document.querySelector("#categories-link").addEventListener("click", () => {
  getAllCategories();
});
